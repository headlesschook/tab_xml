#!/usr/bin/python3
'''
                  20181228
          This is free code
            brought to you by a team of headless chooks
              use it if you can find it
................................................................

 Necessity is mother of all evils
        20181219 shortly before 12 local time
           ubet shuttered tatts.com and took our data
 However the XML site is still alive.
  it contains all the race data. it even contains quinella odds

 in desperation I put this together
  it saves XML in $LOCAL/horses/txml/yyyymmdd/??#.xml

0/   inseert local directory at the beginning of the python exec path

1/ ubet SSL certificates are not in python
        so that is bypassed
ubet use self signed sceritficate

...................................................
2018 12 21
conditional read ubet xml
add xml parse 
2018 12 21 15:30
i have isolated meeting, race,horse, rider, odds, rating,handicap,  l3s
it reads all 1 day

'''
#
# stack overflow 27835619
#
# turn off verification. (for tab.ubet.com)
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
#
from datetime import date
#
import os
from os.path import exists
import regex as re
import sys
from urllib.request import FancyURLopener
from time import sleep
#
class Op3nr( FancyURLopener, object ):
    version = 'Mozilla/5.0 ( Windows: U; Windows NT 5.1; it; rv; 1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
sys.path.insert( 0, os.curdir )

#
#    given key locate value: key="val"
#
def keyval( key, string ):
  ki = string.find( key )
  if ki < 0:
#    print("\n keyval:\n [%s]key not found" %key )
    return None
  vals = len(key)+ki+1
  vale = string[vals:].find( '"' )+vals
#  print( string[vals:vale] )
  return string[vals:vale]
#
#def do_x_
#
#        x_tag locates tag return index or -1
#
def x_tag( tag, xml ):
  rde=0
  if tag[0] == '/':
    ftag = '<%s' %tag
  else:
    ftag = '<%s ' %tag
##  print( "%s%s" %( ftag  ,'--------------------------------------------------------'))
  while not( xml[rde:].lower().startswith( ftag )):
    sde = xml[rde:].find( '>' )
    if sde < 0:
      return sde
    rde += sde + 1
#    print(">>>>>>>>>>>>>>>>>%d<<<<<<<<<<<<<<<<<<<<" %rde)
#    sleep(0.31)
#  print(":===:%s:===:" % xml[rde:rde+11] )
  return rde
#
#      locate tipster section
#
def do_x_tipster( xml ):
  sde = x_tag( 'tipstertip', xml )
  rde = x_tag( '/tipster', xml )
  rde += xml[rde:].find( '>' )+1
#  print( xml[sde:rde] ,'xxxxxxxxxxxxxxxxxxxx')
  tips = keyval( "Tips=", xml[sde:] )
  tipster = keyval( "TipsterName=", xml[sde:])
  if tips == None:
    tips = "None" 
  print( "#TIPSTER,%s,TIPS,%s" %(tipster, tips.replace( "-", ',' )) )
  return rde
#
#         locate fixed odds section
#
def do_x_fixedodds( xml ):
  rde = x_tag( '/fixedodds', xml)
  rde += xml[rde:].find( '>' )+1
#  print("FIXED ODDS:::::", xml[:rde] ,"eeeeeeeeeeeeeeeeee")
  fwin = keyval( "WinOdds=", xml[:rde] )
  print( "#FWIN,%s" % fwin )
  return rde
#
#            tote win odds
#
def do_x_winodds( xml ):
  rde = xml.find( '>' )+1
#  print("#WIN ODDS:", xml[:rde] )
  twin = keyval( "Odds=", xml[:rde] )
  print( "#TWIN,%s" % twin )

  return rde
#
#             tote place odds
#
def do_x_placeodds( xml ):
  rde = xml.find( '>' )+1
#  print("PLACE ODDS:", xml[:rde] )
  tplace = keyval( "Odds=", xml[:rde] )
  print( "#TPLACE,%s" %tplace )
  return rde
#
def do_x_subfav( xml ):
  rde = xml.find( '>' )+1
  print( xml[:rde] )
#
  pass
def do_x_meeting( xml ):
  rde = xml.find( '>' )+1
  print( xml[:rde] )
#
  pass
#
#      runner tag horse #  name scratch rider barrier weight rating
#
def do_x_runner( xml ):
  sde = x_tag( 'runner', xml )
#  print("<<<<<<<<<<<<<<<<<%d>>>>>>>>>>>>>>>>>>>>>>>>>>>>" %sde )
  if sde < 0:
    return sde
  rde = xml[sde:].find( '>' )+1 +sde
#  print("RUNNER:", xml[sde:rde] )
  trug = keyval( "RunnerNo=", xml[sde:rde] )
  horsename = keyval( "RunnerName=", xml[sde:rde] )
  scratched = keyval( "Scratched=", xml[sde:rde] )
  tjok = keyval( "Rider=", xml[sde:rde] )
  tbar = keyval( "Barrier=", xml[sde:rde] )
  twt = keyval( "Weight=", xml[sde:rde] )
  qrat = keyval( "Rtng=", xml[sde:rde] )
  form = keyval( "Form=", xml[sde:rde] )
  l3s = keyval( "LastResult=", xml[sde:rde] )
  print( "#TRUG,%s,HNAME,%s,SCRATCHED,%s,TJOK,%s,TBAR,%s,FORM,%s,L3S,%s,TWT,%s,QRAT,%s" %( trug,horsename,scratched,tjok,tbar,form,l3s,twt,qrat ))
#
#
# win odds
  rde += do_x_winodds( xml[rde:] )
# place odds
  rde += do_x_placeodds( xml[rde:] )
# fixed odds
  rde += do_x_fixedodds( xml[rde:] )
#
#
  rde += x_tag( '/runner', xml[rde:] )
  rde += xml[rde:].find( '>' )+1
  return rde
#
#   race tag race # jump name AKA class distance WEATHER PEN GOING 
#
def do_x_race( xml ):
  rde = xml.find( '>' )+1
#  print( xml[:rde],'.............' )
  rde += x_tag( 'race', xml[rde:] )
#  print(xml[:rde],'==============================')
#  print(xml[rde:rde+44],'$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
  raceno = keyval( "RaceNo=", xml[rde:] )
  rclass = keyval( "RaceName=", xml[rde:] )
  distance = keyval( "Distance=", xml[rde:] )
  going = keyval( "TrackDesc=", xml[rde:] )
  pen = keyval( "TrackRating=", xml[rde:] )
  print( "#RACENO,%s,CLASS,%s,DISTANCE,%s,GOING,%s,PEN,%s" %( raceno, rclass, distance, going, pen ))
#  exit()
#  exit()
  rde += do_x_tipster( xml[rde:] )
#  print('===================',xml[rde:rde+22],'=====================')
#  exit()
#  print("*********************")
#  sde = rde

  while rde != None:
    sde = rde
  #
  #   .find returns -1 if not found
  #
    rde += do_x_runner( xml[rde:] )
    if rde < sde:
      break
#  print("******************+++++++++++++++++++++++++============+(**********************")
#  exit()
  return sde
#
def do_x_pool( xml ):
#  print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  rde = xml.find( '>' )+1
#  print( xml[:rde],'::::::::::::::::::::::' )
  while not( xml[rde:].lower().startswith( '</pool>' )):
    rde = xml[rde:].find( '>' )+1 + rde
  rde = xml[rde:].find( '>' )+1 + rde
  return rde
  pass
#
#
def do_x_meet( xml ):
  rde = xml.find( '>' )+1
#
#  print( xml[:rde] )
#  exit()
#'........................................' )
#  print("*************************")
#  print( xml[rde:rde+14] )
#  print("*************************")
  qcode = keyval( "MeetingCode=", xml[:rde] )
  venue = keyval( 'VenueName=', xml[:rde] )
  keyval( 'MtgType=', xml[:rde] )
  going = keyval( 'TrackDesc=', xml[:rde] )
  pen = keyval( 'TrackRating=', xml[:rde] )
  weather = keyval( 'WeatherDesc=', xml[:rde] )
  print( "#QCODE,%s,VENUE,%s,GOING,%s,PEN,%s,WEATHER,%s" %( qcode,venue,going,pen,weather ))
###  exit()
  while xml[rde:].lower().startswith( '<pool ' ):
    rde = rde + do_x_pool( xml[rde:] )
#    print( 'POOL ::::::::::::::::::::::::::::',xml[rde:rde+14] )
#  exit()
  print("#=============================================================================")
#  print( rde,xml[rde:rde+11],rde )
#  rde = x_tag( 'race', xml[rde:] ) + rde
#  print(rde,xml[rde:rde+12],rde)
  rde += do_x_race( xml[rde:] )
#
#  print('... sub fav section ...................',xml[rde:rde+43],'..........................')
#  exit()
#
#          load raceday yyyy mm yy
#
def do_x_raceday( xml ):
  rde = xml.find( '>' )+1
######  print( xml[:rde] )
#
  ys,ye = re.search( 'Year="\d{4}', xml[:rde],flags=re.I ).span()
  ms,me = re.search( 'Month="\d{1,2}', xml[:rde],flags=re.I ).span()
  ds,de = re.search( 'Day="\d{1,2}', xml[:rde],flags=re.I ).span()
#
  yyyy = xml[ye-4:ye]
  mm = xml[me-2:me]
  dd = xml[de-2:de]
  if not mm[0].isdigit(): mm = "0%c" % mm[1]
  if not dd[0].isdigit(): dd = "0%c" % dd[1]
  dom = "%s%s%s" %( yyyy,mm,dd )
  qdom = "%s-%s-%s" %( yyyy,mm,dd )
  print( "#dom,%s,qdom,%s" %( dom,qdom ))
#  print( yyyy,dd,mm,dom,qdom )
#
#      get 
#
  do_x_meet( xml[rde:] )
  print("#       COMPLETE")
  return True
  exit()
  while do_x_race( xml ) != '</RaceDay>': continue
  return True
  pass
#
#
#
def do_x_( xml ):
  return do_x_raceday( xml )
#
# date of meeting used by tatts
#
SITE="https://tatts.com/pagedata/racing"
DOM = date.today().strftime( '%Y/%m/%d' )
#
#      storage date used by me yyyymmdd
#
STODA = date.today().strftime( '%Y%m%d' )
CODES = [ 'BR', 'QR', 'SR', 'NR', 'MR', 'VR', 'AR', 'CR', 'ZR', 'ER', 'OR', 'XR', 'BG', 'QG', 'SG', 'NG', 'MG', 'VG', 'AG', 'CG', 'ZG' ]
LOCAL = "/usr/local/share/public/"
HORSE = "%s/horses" %( LOCAL )
XMLST = "%s/txml/%s/" %( HORSE, STODA )
#!/bin/sh
#
#          short url, less race code and race number
#
SURL="%s/%s" %( SITE, DOM )

opener = Op3nr()
if not exists( XMLST ):
   os.makedirs( XMLST )
for CODE in CODES:
  for RACE in [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12' ]:
    RACO="%s%s.xml" %( CODE, RACE )
#  OUT="$RACO.
    URL="%s/%s" %( SURL, RACO )
#    print(URL)
    #
    #
    if not exists( "%s/%s" %( XMLST, RACO )):
      qin = opener.open( URL )
      try:
        xml = qin.read().decode()
      except:
        qin.close()
        break
      qin.close()
#    exit()
#      continue
      fo = open( "%s/%s" %( XMLST, RACO ), "w" )
      fo.write( xml )
      fo.close()
#
#   this else bit may not be what you need
#
    else:
      fi = open( "%s/%s" %( XMLST, RACO ), "r" )
      xml = fi.read( )
      fi.close()
    if CODE[1] == 'G':
      continue
#    print(xml)
#    exit()
    do_x_( xml )
#    print( RACO )
#    print( URL  )
#
#    exit()
#        enable this exit for testing 1 race
#
#
exit()  
 #
#


