#!/usr/bin/python3
'''
                    20181228
      this is free code
        brought to you by a team of headless chooks
          if you find it, you can use it

................................................
qtab XML feed to hr.prep.py
1/ qgaxml.py
             grab tab xml
2/ hr.load.form.py
             grab formguides
3/ hr.gxml.py
            convert tab xml into raw csv
4/ hr.pox.py
            This is stage 4, raw csv to prepy
tab is using self signed certificate
python hates this. disable is a BAD workaround
but it works

'''
#
#			import regex instead of re. regex more stable
#
import ssl
from urllib.request import FancyURLopener
#
class Op3nr( FancyURLopener, object ):
    version = 'Mozilla/5.0 ( Windows: U; Windows NT 5.1; it; rv; 1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'
# nutshell p 246 and beyond
#
import os
from os.path import exists
#
#import regex as re
import re
#
import sys
#
from time import sleep
from datetime import date

TODAY = date.today().strftime( '%Y-%m-%d' )
YEAR  = date.today().strftime( '%Y' )
MONTH  = date.today().strftime( '%m' )
DAY  = date.today().strftime( '%d' )
#
#
Local = '/usr/local/share/public/'
DESTDIR = Local + 'horses/xprepy/' + YEAR + '/'
#
# 2018 12 28 testing -->> old prepy
#
DESTDIR = Local + 'horses/prepy/' + YEAR + '/'
#
#url = "https://tattsbet.ubet.com/racing"
#url = "https://tatts.com/racing"
#
BAD_CODES = [  'DR','FR','IR','OR','PR','TR','XR' ]
BAD_TRACKS = [  ]
#-------------------------------------------------------------------------------------------------------------
#
def ganl( text ):
  '''
   return next line, pos of line after that one
  '''
  tei = text.find( '\n' )
  return text[ :tei ], tei+1
#
#-------------------------------------------------------------------------------------------------------------
#
respane = re.compile( r'</span>', flags=re.I)
#
#-------------------------------------------------------------------------------------------------------------
#
def print_race( meta, da_race, fop ):
  doom = "%s/%s/%s" %( meta[ 'dom' ][:4], meta[ 'dom' ][4:6], meta[ 'dom' ][6:] )
  heads = "%s/%s/%s,%s,%s,%s,%s,%s" %( doom, meta[ 'qcode' ], da_race[ 'raceno' ], da_race[ 'distance' ], meta[ 'weather' ], meta[ 'going' ], meta[ 'pen' ], meta[ 'venue' ] )
#  print( "%s/%s/%s,%s,%s,%s,%s" %( meta[ 'dom' ], meta[ 'qcode' ], da_race[ 'raceno' ], da_race[ 'distance' ], meta[ 'weather' ], meta[ 'pen' ], meta[ 'venue' ] ))#,
  print( heads )
  fop.write( heads )
  fop.write( '\n' )
  for nho in range(len( da_race[ 'horse' ] )):
#    print( nho )
    ho = da_race[ 'horse' ][nho]
#    print( da_race[ 'horse' ][nho] )
    form = ho[ 'form' ].replace( 'None', " " )
    odds = "%.2f" %float( ho[ 'fwin' ] )
    hname = ho[ 'hname' ].replace( "'", '^' )
    tjok = ho[ 'tjok' ].replace( "'", '^' )
    qrat = ho[ 'qrat' ]
    if qrat == 'None':
      qrat = 11
    run_data = "%s,%s,%s,%s,%s,%s,%s,%s,%s" %( ho[ 'hnumber' ], hname, tjok, ho[ 'l3s' ], form, ho[ 'twt' ], qrat, odds, ho[ 'tbar' ] )
#    print( "%s,%s,%s,%s,%s,%s,%s,%f,%s" %( ho[ 'hnumber' ], ho[ 'hname' ], ho[ 'tjok' ], ho[ 'l3s' ], form, ho[ 'twt' ], ho[ 'qrat' ], ho[ 'fwin' ], ho[ 'tbar' ] ))
#    print( "%s,%s,%s,%s,%s,%s%s,%s,%s" %( ho[ 'hnumber' ], ho[ 'hname' ], ho[ 'tjok' ], ho[ 'l3s' ], ho[ 'form' ], ho[ 'twt' ], ho[ 'qrat' ], ho[ 'fwin' ], ho[ 'tbar' ] ))
    print( run_data )
    fop.write( run_data )
    fop.write( '\n' )
#
def addahorse( txt, horse ):
  scratched = 'SCRATCHED'
  scrodds = '999.99'
  '''
  horse number name rider(scratched) tbar form l3s twt qrat
  and
  tote win
  tote place
  fixed win
  '''
  htxt = txt.split( ',' )
####################  print(htxt)
  if htxt[0] == "#TWIN":
    horse[ 'twin' ] = htxt[ 1 ]
    return horse
  if htxt[0] == "#TPLACE":
    horse[ 'tplace' ] = htxt[ 1 ]
    return horse
  if htxt[0] == "#FWIN":
    horse[ 'fwin' ] = htxt[ 1 ]
    if horse[ 'tjok' ] == scratched:
      horse[ 'twin' ] = scrodds
      horse[ 'tplace' ] = scrodds
      horse[ 'fwin' ] = scrodds
      horse[ 'qrat' ] = 11
    return horse
  if htxt[0] != "#TRUG":
################    print("proble here looking for a horse ")
###############    print( htxt )
    return htxt
  horse = {}
  horse[ 'hnumber' ] = htxt[ 1 ]
  horse[ 'hname' ] = htxt[ 3 ]
  horse[ 'tjok' ] = htxt[ 7 ]
  horse[ 'tbar' ] = htxt[ 9 ]
  horse[ 'form' ] = htxt[ 11 ]
  horse[ 'l3s' ] = htxt[ 13 ]
  if htxt[ 13 ] == 'None':
    horse[ 'l3s' ] = ' '
  horse[ 'twt' ] = htxt[ 15 ]
  horse[ 'qrat' ] = htxt[ 17 ]
  horse[ 'twin' ] = None
  horse[ 'tplace' ] = None
  horse[ 'fwin' ] = None
  if htxt[ 5 ] == 'Y':
    horse[ 'tjok' ] = scratched
#  print( horse )
  return horse
#
def addarace( txt ):
  rameta = ""
  ra_meta = {}
  txp = 0
  while "#RACENO," not in rameta:
    rameta,txc  = ganl( txt[ txp: ] )
    txp += txc
  rameta = rameta.split( ',' )
  ra_meta[ 'raceno' ] = rameta[ 1 ]
  ra_meta[ 'class' ] = rameta[ 3 ]
  ra_meta[ 'distance' ] = rameta[ 5 ]
  ra_meta[ 'going' ] = rameta[ 7 ]
  ra_meta[ 'pen' ] = rameta[ 9 ]
  tips,txc = ganl( txt[ txp: ] )
  txp += txc
  ra_meta[ 'tips' ] = tips
  ra_meta[ 'horse'] = []
  complete = ""
  horse = {}
  while " COMPLETE" not in complete:
    complete,txc = ganl( txt[ txp: ] )
    txp += txc
    ho = addahorse( complete, horse )
    if " COMPLETE" in ho:
      continue
    horse = ho
    if "#FWIN" in complete:
#      print(horse,'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
      ra_meta[ 'horse' ].append( horse )
  return ra_meta, txp
########
  print( ra_meta,'RA META' )
  print_race( ra_meta )
  sleep(33)
  return
#####################
#
def get_meta( txt, meta ):
  '''
  add qcode, venue, weather, pen, going to meta
  '''
  codeline = ""
  txp = 0
  while "#QCODE," not in codeline:
    codeline, tx = ganl( txt[txp:] )
    txp += tx
  codes = codeline.split( ',' )
  meta[ 'qcode' ] = codes[ 1 ]
  meta[ 'venue' ] = codes[ 3 ]
  meta[ 'going' ] = codes[ 5 ]
  meta[ 'pen' ] = codes[ 7 ]
  meta[ 'weather' ] = codes[ 9 ]
  return meta, txp
#
#-------------------------------------------------------------------------------------------------------------
#
'''			BEGIN HERE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   '''
#
#-------------------------------------------------------------------------------------------------------------
#
def main( fop ):
  WORKDIR = "/tmp/hox"
  fip = open( "%s/%s" %( WORKDIR, "2da.txt" ), "r" )
  txt = fip.read()
  fip.close()
  meta = {}
  dom = ""
  txp = 0
  while "#dom," not in dom:
    dom,txp = ganl( txt[txp:] )
  #
  while "#dom," in dom:
    meta[ 'dom' ] = dom.split( ',' )[1]
#  print(dom)
    meta, tx = get_meta( txt[txp:], meta )
    txp += tx
#  print(meta)
    da_race, txc = addarace( txt[txp:] )
    print_race( meta, da_race, fop )
    txp += txc
#    print( txt[txp:txp+333] )
    dom,txc = ganl( txt[txp:] )
    txp += txc
  return
#
if __name__ == "__main__":
  sys.path.insert( 0, os.curdir )
#
#     stack overflow 27835619 turn off verification
#
#          this is bad
#           but tab.ubet uses self signed certificate
#
  ssl._create_default_https_context = ssl._create_unverified_context
  if not exists( DESTDIR ):
    os.makedirs( DESTDIR )
  outfile = "%s%s.%s.%s.txt" %( DESTDIR, 'hgo', MONTH, DAY )
  fout = open( outfile, 'w' )
  main( fout )
  fout.close()
  exit()
exit()

